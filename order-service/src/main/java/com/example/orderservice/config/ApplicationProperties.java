/* Licensed under Apache-2.0 2021-2022 */
package com.example.orderservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application")
public record ApplicationProperties() {}
