
How to access the values of particular project

http://localhost:8888/{projectname}/{profile}
 
Ex: http://localhost:8888/catalog-service/default

## Actuator Endpoints
 - http://localhost:8888/actuator/health
 - http://localhost:8888/actuator/info (done using maven build plugin and git info plugin)
 - http://localhost:8888/actuator/health/configServer
